# frozen_string_literal: true

# Handlers for client ws for displaying consumed msg from Kafka
class MessageChannel < ApplicationCable::Channel
  def subscribed
    stream_from 'kafka_consumer'
  end

  def unsubscribed
    stop_stream_for 'kafka_consumer'
    @kafka_consumer&.stop
  end

  def receive_message
    @kafka_consumer = ConsumeFromKafka.new(kafka_consumer: KafkaClient.consumer(group_id: 'chat_room_msgs'),
                                           topics: ['chat_room'])
    @kafka_consumer.call do |message|
      ActionCable.server.broadcast('kafka_consumer', { body: message })
    end
  end
end
