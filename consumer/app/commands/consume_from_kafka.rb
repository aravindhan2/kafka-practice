# frozen_string_literal: true

# Consume messages from Kafka
class ConsumeFromKafka
  def initialize(kafka_consumer:, topics:)
    @kafka_consumer = kafka_consumer
    @kafka_consumer.subscribe(*topics)
  end

  def call
    @kafka_consumer.each_message do |message|
      yield JSON.parse(message.value) if block_given?
    end
  end

  def stop
    @kafka_consumer.stop
  end
end
