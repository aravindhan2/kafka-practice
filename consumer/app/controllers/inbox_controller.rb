# frozen_string_literal: true

# Handler for displaying the kafka consumed messages.
class InboxController < ApplicationController
  def index
  end
end
