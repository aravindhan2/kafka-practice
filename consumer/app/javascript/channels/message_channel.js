import consumer from "./consumer"

consumer.subscriptions.create("MessageChannel", {
  connected() {
    // Called when the subscription is ready for use on the server
    console.log('connected')
    this.receiveMessage()
  },

  disconnected() {
    // Called when the subscription has been terminated by the server
  },

  received(data) {
    // Called when there's incoming data on the websocket for this channel
    console.log("Received", data.body)
    displayMessage(data.body)
  },

  receiveMessage(){
  	this.perform('receive_message')
  },

  room: "kafka_consumer"
});

function displayMessage(data){
	let newMessageElement = document.createElement("div")
	newMessageElement.innerHTML = `Message: ${data.content} At: ${new Date(data.published_at)}`
	document.body.appendChild(newMessageElement)
}

window.onunload = (event) => {
  console.log('The page is unloaded')
  consumer.disconnect()
};