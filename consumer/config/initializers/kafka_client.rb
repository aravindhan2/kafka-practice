# frozen_string_literal: true

# Manages the kafka consumer instance
class KafkaClient
  def self.consumer(group_id:)
    @client ||= Kafka.new(ENV['KAFKA_ENDPOINT'], logger: Rails.logger)
    @client.consumer(group_id: group_id)
  end
end
