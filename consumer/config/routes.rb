# frozen_string_literal: true

Rails.application.routes.draw do
  root to: 'inbox#index'

  resources :inbox, only: [:index]
end
