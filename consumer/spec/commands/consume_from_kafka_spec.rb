# frozen_string_literal: true

require 'rails_helper'

describe ConsumeFromKafka do
  subject(:consumer_client) { described_class.new(kafka_consumer: kafka_consumer, topics: 'messages') }

  let(:kafka_message) { instance_double(Kafka::FetchedMessage) }

  let(:kafka_consumer) { instance_double(Kafka::Consumer) }

  before do
    allow(kafka_consumer).to receive(:subscribe).with(any_args)
  end

  describe '#call' do
    before do
      allow(kafka_consumer).to receive(:each_message).and_yield(kafka_message)
    end

    it 'calls kafka consumer interface with block' do
      consumer_client.call

      expect(kafka_consumer).to have_received(:each_message)
    end

    it 'executes the block if passed' do
      allow(kafka_message).to receive(:value).and_return({ content: 'Hi' }.to_json)

      expect do |block|
        consumer_client.call(&block)
      end.to yield_with_args(Hash)
    end
  end

  describe '#stop' do
    it 'stop receiving message from kafka' do
      allow(kafka_consumer).to receive(:stop)

      consumer_client.stop

      expect(kafka_consumer).to have_received(:stop)
    end
  end
end
