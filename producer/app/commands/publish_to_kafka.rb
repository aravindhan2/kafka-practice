# frozen_string_literal: true

# Communicates with Kafka server for publishing the messages
class PublishToKafka
  def initialize(kafka_producer:, topic:)
    @kafka_producer = kafka_producer
    @topic = topic
  end

  def call(message:)
    @kafka_producer.produce(message, topic: @topic)
    @kafka_producer.deliver_messages
  end
end
