# frozen_string_literal: true

# Manages the messages received from the user for publishing to Kafka
class MessagesController < ApplicationController
  def index
  end

  def create
    content = params.permit(:message)['message']

    if content.blank?
      render :index, status: :unprocessable_entity
      return
    end

    kafka_publisher = PublishToKafka.new(kafka_producer: KafkaClient.producer, topic: 'chat_room')
    kafka_publisher.call(message: prepare_content_for_kafka_publish(content).to_json)

    flash[:notice] = 'Your message is posted!'
    redirect_to messages_path
  end

  private

  def prepare_content_for_kafka_publish(content)
    {
      content: content,
      published_at: Time.zone.now
    }
  end
end
