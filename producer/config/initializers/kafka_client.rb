# frozen_string_literal: true

# Manages the kafka publishing instance
class KafkaClient
  def self.producer
    @client ||= Kafka.new(ENV['KAFKA_ENDPOINT'], logger: Rails.logger)
    @client.producer
  end
end
