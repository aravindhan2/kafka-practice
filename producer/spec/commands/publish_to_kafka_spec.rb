# frozen_string_literal: true

require 'rails_helper'

describe PublishToKafka do
  subject(:kafka_publisher) { described_class.new(kafka_producer: kafka_producer, topic: topic) }

  let(:kafka_producer) { instance_double(Kafka::Producer) }
  let(:topic) { 'chatroom' }

  describe '#call' do
    before do
      allow(kafka_producer).to receive(:produce).with(anything, hash_including(topic: instance_of(String)))
      allow(kafka_producer).to receive(:deliver_messages)
    end

    context 'when topic is present' do
      it 'publishes the message to kafka' do
        kafka_publisher.call(message: 'Hi.')

        expect(kafka_producer).to have_received(:produce).with(anything, hash_including(topic: instance_of(String)))
        expect(kafka_producer).to have_received(:deliver_messages)
      end
    end
  end
end
