# frozen_string_literal: true

require 'rails_helper'
describe MessagesController, type: :request do
  subject(:submit_form) do
    post '/messages', params: form_parameters do
      header 'Content-Type', 'application/x-www-form-urlencoded'
    end
  end

  before do
    ActionController::Base.allow_forgery_protection = false
  end

  let(:form_parameters) do
    { message: 'Hello world' }
  end

  describe 'POST create' do
    context 'when message is passed' do
      it 'publishes the message' do
        submit_form

        expect(response).to have_http_status(:found)
        expect(response).to redirect_to(messages_url)
      end
    end

    context 'when message is blank' do
      let(:form_parameters) { {} }

      it 'returns unprocessable entity status' do
        submit_form

        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end
